import pandas as pd
import numpy as np

class Data:
    """Loads data from CSV and parses into relevant numpy arrays.
        A single instance is created in the model."""

    def __init__(self):
        """Constructor method for data."""
        
        self.read_input_csv()

        self.maximum_booking_window = int(self.get_maximum_booking_window())

        self.model_warm_up_period = int(self.maximum_booking_window)

        self.data_collection_weeks = 100

        self.model_duration = \
            int(self.model_warm_up_period +self.data_collection_weeks * 7)

        self.diary_length = int(self.model_duration + self.maximum_booking_window)

        self.mean_calls_by_day = self.set_up_arrivals_by_day()
        
        self.demand_type_cum_probability = \
            self.set_up_demand_type_cumulative_probabilities()
            
        self.triage_use_probability = self.set_up_triage_use_probabilities()
        
        self.triage_conversion_probabilities = \
            self.set_up_triage_conversion_probabilities()

        self.appointment_durations = self.set_up_appointment_duration()
        
        self.appointment_booking_windows = self.set_up_appointment_windows()
        
        self.gp_hours = self.set_up_GP_hours()
        
        self.reserved_diary_by_type = self.set_up_reserved_diary()

        self.overtime_for_triage = \
            self.input.loc['Prop triage using overtime'].values

        self.overtime_for_same_day = \
            self.input.loc['Prop same day using overtime'].values

        self.failed_demand_return_proportion = \
            self.input.loc['Prop failed demand who call next day'].values
        
        return
    
    
    def get_maximum_booking_window(self):
        """Find largest advanced booking window"""
        
        rows=['Same day maximum appointment window',
              'Short term maximum appointment window',
              'Long term maximum appointment window',
              'Visit maximum appointment window']

        return np.max(self.input.loc[rows].values)
    
    def read_input_csv(self):
        """Reads input csv into pandas data frame"""
        
        self.input = pd.read_csv('input.csv', index_col="MODEL PARAMETER")

        return


    def set_up_appointment_duration(self):
        """Set up NumPy array of appointment duration by appointment type"""
        
        rows = ['Triage duration',
                'Same day duration',
                'Short term duration',
                'Long term duration',
                'Visits duration']
        durations = self.input.loc[rows].values.flatten()
        durations /= 60

        return durations
    
    
    def set_up_appointment_windows(self):
        """Set up NumPy array of appoint booking windows.
        Col 0: First allowed day (from day of booking)
        Col 1: Last allowed day (from day of booking)"""
        
        array = np.zeros((4,2))
        
        rows = ['Same day minimum appointment window',
                'Short term minimum appointment window',
                'Long term minimum appointment window',
                'Visit minimum appointment window']
        
        array[:, 0] = self.input.loc[rows].values.flatten()
        
        rows = ['Same day maximum appointment window',
                'Short term maximum appointment window',
                'Long term maximum appointment window',
                'Visit maximum appointment window']
        
        array[:, 1] = self.input.loc[rows].values.flatten()
        
        return array 
        
    
    def set_up_arrivals_by_day(self):
        """Create NumPy array of mean arrivals by weekday"""
        
        rows = ['Demand proportion Monday',
                'Demand proportion Tuesday',
                'Demand proportion Wednesday',
                'Demand proportion Thursday',
                'Demand proportion Friday',
                'Demand proportion Saturday',
                'Demand proportion Sunday']
        prop_by_day = self.input.loc[rows]
        calls_per_day = prop_by_day * self.input.loc['Contacts per week']
        calls_per_day = calls_per_day.values.flatten()


        return calls_per_day


    def set_up_demand_type_cumulative_probabilities(self):
        """Return NumPy array of cumulative probabilities of demand type"""
        
        rows = ['Same day',
                'Short term',
                'Long term',
                'Visits']
        probabilities = np.cumsum(self.input.loc[rows].values.flatten())

        return probabilities
    

    def set_up_GP_hours(self):
        """Return NumPy array of GP hours by day of week"""
        
        rows = ['GP contact hours Monday',
                'GP contact hours Tuesday',
                'GP contact hours Wednesday',
                'GP contact hours Thursday',
                'GP contact hours Friday',
                'GP contact hours Saturday',
                'GP contact hours Sunday']
        hours_by_day = self.input.loc[rows].values.flatten()

        return hours_by_day
        
       
    def set_up_reserved_diary(self):
        """Return NumPy array of proportion of diary reserved by appointment 
        type"""
        
        rows = ['Triage reserved diary',
                'Same day reserved diary',
                'Short term reserved diary',
                'Long term reserved diary',
                'Visits reserved diary']
        
        reserved = self.input.loc[rows].values.flatten()
        unreserved = 1 - np.sum(reserved)
        reserved = np.append(reserved, unreserved)

        
        return reserved
    
    
    def set_up_triage_conversion_probabilities(self):
        """Return NumPy array of probabilities of triage conversion"""
        
        rows = ['Same day conversion',
                'Short term conversion',
                'Long term conversion',
                'Visits conversion']
        probabilities = self.input.loc[rows].values.flatten()
        
        return probabilities
        
        
    def set_up_triage_use_probabilities(self):
        """Return NumPy array of probabilities of use of triage"""
        
        rows = ['Same day triage',
                'Short term triage',
                'Long term triage',
                'Visits triage']
        probabilities = self.input.loc[rows].values.flatten()

        return probabilities
        
        
