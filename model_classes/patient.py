import random as rn


class Patient:
    """
    Patients are instances of the patient class.
    """

    def __init__(self,
                 patient_id,
                 appointment_type_cum_prob,
                 triage_use_probability,
                 triage_conversion_probabilities,
                 appointment_duration,
                 overtime_for_triage,
                 overtime_for_same_day,
                 return_next_day):

        """Constructor method for Patients"""

        # Unique patient ID
        self.id = patient_id

        # Set appointment type excluding triage
        self.apppointment_type = \
            self.set_appoitment_type(appointment_type_cum_prob)

        # Set triage use and conversion
        self.use_triage = \
            self.set_use_triage(self.apppointment_type, triage_use_probability)

        # Set triage conversion to F2F
        self.triage_conversion = \
            self.set_triage_conversion(self.apppointment_type,
                                       triage_conversion_probabilities)

        # Set appointment duration (excluding triage), and change to hours
        self.f2f_duration = \
            self.set_f2f_duration(self.apppointment_type, appointment_duration)

        self.triage_duration = appointment_duration[0]

        # Number of attemps to get appointment
        self.attempts = 1

        # Set failed to get appointment
        self.failed_to_get_appointment = 0

        # Set failed demand will return next day
        self.return_next_day = self.set_return_next_day(return_next_day)

        # Set use of overtime
        self.overtime_for_triage = \
            self.set_overtime_for_triage(overtime_for_triage)

        self.overtime_for_same_day = \
            self.set_overtime_for_same_day(overtime_for_same_day)

        return

    def set_appoitment_type(self, appointment_type_cum_prob):
        """Set appointment type"""

        x = rn.random()
        for index, cutoff in enumerate(appointment_type_cum_prob):
            if x <= cutoff:
                type = index
                break

        return type


    def set_f2f_duration(self, apppointment_type, appointment_duration):
        """Set f2f duration"""

        # Increment apppointment_type to skip triage duration
        duration = appointment_duration[apppointment_type + 1]

        return duration


    def set_overtime_for_same_day(self, overtime_for_same_day):
        """Set use overtime for same day appointment (0 or 1)"""
        x = rn.random()
        cutoff = overtime_for_same_day
        overtime = 0
        if x <= cutoff:
            overtime = 1

        return overtime


    def set_overtime_for_triage(self, overtime_for_triage):
        """Set use overtime for triage (0 or 1)"""
        x = rn.random()
        cutoff = overtime_for_triage
        overtime = 0
        if x <= cutoff:
            overtime = 1

        return overtime

    def set_return_next_day(self, return_proportion):
        """Set whether failed demand will reyurn next day"""
        x = rn.random()
        cutoff = return_proportion
        return_next_day = 0
        if x <= cutoff:
            return_next_day = 1

        return return_next_day


    def set_triage_conversion(self,
                              appointment_type,
                              triage_conversion_probabilities):
        """Set triage to F2F conversion (0 or 1)"""
        x = rn.random()
        cutoff = triage_conversion_probabilities[appointment_type]
        conversion = 0
        if x <= cutoff:
            conversion = 1

        return conversion


    def set_use_triage(self, appointment_type, triage_use_probability):
        """Set whether triage will be used (0 or 1"""

        x = rn.random()
        cutoff = triage_use_probability[appointment_type]
        use_triage = 0
        if x <= cutoff:
            use_triage = 1

        return use_triage
