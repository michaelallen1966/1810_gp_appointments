import numpy as np

class Appointment_diary:
    """
    Class for holding scheduled appointments (diary) with methods to book
    patients in for appointments.
    """

    def __init__(self, days, gp_hours, reserved_proportion):
        """Set up NumPy tables for:

        GP unused hours
        GP used hours
        Number of booked appointments
        Number of rejected appointments

        For each table each row is one day in the model. The six columns are:

        Col 0: Triage
        Col 1: Same day
        Col 2: Short term
        Col 3: Long term
        Col 4: Visits
        Col 5: Unreserved hours (for GP unused hours only)

        Additionally GP overtime per day is looged in gp_overtime
        """

        self.gp_unused_hours = \
            self.set_up_gp_unused_hours(days, gp_hours, reserved_proportion)
        self.gp_used_hours = np.zeros((days, 5))
        self.gp_booked_appointments = np.zeros((days, 5))
        self.gp_rejected_appointments = np.zeros((days, 5))
        self.gp_overtime = np.zeros(days)

        return


    def assign_appointment(self, day, patient, appointment_booking_windows):
        """Try to assign appointment"""

        # If triage used .....
        if patient.use_triage == 1:
            # Check GP time available
            if (self.gp_unused_hours[day, 0] > patient.triage_duration or
                    self.gp_unused_hours[day, 5] > patient.triage_duration):

                self.gp_booked_appointments[day, 0] += 1
                self.gp_used_hours [day, 0] += patient.triage_duration
                patient.failed_to_get_appointment = 0

                # adjust GP available time
                if self.gp_unused_hours[day, 0] > patient.triage_duration:
                    self.gp_unused_hours[day, 0] -= patient.triage_duration
                else:
                    self.gp_unused_hours[day, 5] -= patient.triage_duration
            # GP time is not available - check whether overtime can be used
            else:
                if patient.overtime_for_triage == 1:
                    self.gp_booked_appointments[day, 0] += 1
                    self.gp_overtime[day] += patient.triage_duration
                    self.gp_used_hours[day, 0] += patient.triage_duration
                    patient.failed_to_get_appointment = 0
                else:
                    self.gp_rejected_appointments[day, 0] += 1
                    patient.failed_to_get_appointment = 1
                    return
            # Return if triage does not lead to F2F
            if patient.triage_conversion == 0:
                return

        # Face to face appointments; Adjust patient appointment type for triage
        appointment_type = patient.apppointment_type + 1
        
        # Same day appointments only
        if appointment_type == 1:
           # Check GP time available
            if (self.gp_unused_hours[day, appointment_type] >
                    patient.f2f_duration or
                    self.gp_unused_hours[day, 5] > patient.f2f_duration):
                
                # Time available
    
                self.gp_booked_appointments[day, appointment_type] += 1

                self.gp_used_hours[day, appointment_type] += \
                    patient.f2f_duration

                patient.failed_to_get_appointment = 0
    
                # adjust GP available time
                if self.gp_unused_hours[day, appointment_type] > \
                        patient.f2f_duration:

                    self.gp_unused_hours[day, appointment_type] -= \
                        patient.f2f_duration
                else:
                    self.gp_unused_hours[day, 5] -= patient.f2f_duration
    
            # GP time is not available -
            # check whether overtime can be used (same day only)

            else:
                if appointment_type == 1 and patient.overtime_for_same_day == 1:
                    self.gp_booked_appointments[day, appointment_type] += 1
                    self.gp_overtime[day] += patient.f2f_duration
                    self.gp_used_hours[day, appointment_type] += \
                        patient.f2f_duration
                    patient.failed_to_get_appointment = 0
                    return
                else:
                    self.gp_rejected_appointments[day, appointment_type] += 1
                    patient.failed_to_get_appointment = 1
                    # Set no triage needed if patient calls again next day
                    patient.use_triage = 0
                    return
                
        # Advanced booking
        else:
            min_diary_day = \
                int(day + appointment_booking_windows[appointment_type -1, 0])
            max_diary_day = \
                int(day + appointment_booking_windows[appointment_type -1, 1])
            # Look for first day with available time
            appointment_found = 0
            for lookup_day in range(min_diary_day, max_diary_day+1):
                if (self.gp_unused_hours[lookup_day, appointment_type] >
                        patient.f2f_duration or
                    self.gp_unused_hours[lookup_day, 5] > patient.f2f_duration):
                
                    # Time available   
                    appointment_found = 1
                    self.gp_booked_appointments[lookup_day, appointment_type] \
                        += 1
                    self.gp_used_hours[lookup_day, appointment_type] \
                        += patient.f2f_duration
                    
                    # adjust GP available time
                    if self.gp_unused_hours[lookup_day, appointment_type] > \
                            patient.f2f_duration:
                        self.gp_unused_hours[lookup_day, appointment_type] \
                            -= patient.f2f_duration
                    else:
                        self.gp_unused_hours[lookup_day, 5] -= \
                            patient.f2f_duration
                    break                        
            
            if appointment_found == 1:
                patient.failed_to_get_appointment = 0
            else:
                # No appintment found
                self.gp_rejected_appointments[day, appointment_type] += 1
                patient.failed_to_get_appointment = 1
                # Set no triage needed if patient calls again next day
                patient.use_triage = 0
            
            return


    def set_up_gp_unused_hours(self, days, gp_hours, reserved_proportion):
        """Set up appoitnment diary with available GP hours"""
        weekday = 0
        array = np.zeros((days, 6))
        for day in range(days):
            total_hours = gp_hours[weekday]
            array[day] = total_hours * reserved_proportion
            weekday += 1
            if weekday == 7:
                weekday = 0

        return array




