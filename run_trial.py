import pandas as pd
import numpy as np
from model_classes import model

# Result lists
patient_count = []
demand_met = []
demand_met_first_time = []
demand_failed = []
available_gp_hours_no_overtime = []
used_gp_hours = []
used_gp_overtime = []
prop_failed_triage = []
prop_failed_same_day = []
prop_failed_short_term = []
prop_failed_long_term = []
prop_failed_visit = []
prop_used_gp_time_triage = []
prop_used_gp_time_same_day = []
prop_used_gp_time_short_term = []
prop_used_gp_time_long_term = []
prop_used_gp_time_visit = []



number_of_runs = 5

for run in range(number_of_runs):

    model_run = model.Model()

    patient_count.append(model_run.results_patient_count)

    demand_met.append(model_run.demand_met)

    demand_met_first_time.append(model_run.demand_met_first_time)

    demand_failed.append(model_run.demand_failed)

    available_gp_hours_no_overtime.append(model_run.data.gp_hours.sum())

    (used_gp_hours.append(model_run.results_total_gp_hours_used /
                                      model_run.data.data_collection_weeks))

    (used_gp_overtime.append(model_run.results_total_gp_overtime_hours_used /
                             model_run.data.data_collection_weeks))

    prop_failed_triage.append(
        model_run.results_proportion_of_appointments_failed_by_type[0])

    prop_failed_same_day.append(
        model_run.results_proportion_of_appointments_failed_by_type[1])

    prop_failed_short_term.append(
        model_run.results_proportion_of_appointments_failed_by_type[2])

    prop_failed_long_term.append(
        model_run.results_proportion_of_appointments_failed_by_type[3])

    prop_failed_visit.append(
        model_run.results_proportion_of_appointments_failed_by_type[4])

    prop_used_gp_time_triage.append(
        model_run.results_proportion_used_GP_time_used_by_type[0])

    prop_used_gp_time_same_day.append(
        model_run.results_proportion_used_GP_time_used_by_type[1])

    prop_used_gp_time_short_term.append(
        model_run.results_proportion_used_GP_time_used_by_type[2])

    prop_used_gp_time_long_term.append(
        model_run.results_proportion_used_GP_time_used_by_type[3])

    prop_used_gp_time_visit.append(
        model_run.results_proportion_used_GP_time_used_by_type[4])



    print ('Run %d out of %d complete' %(run +1, number_of_runs))

# Transfer results o pandas datafame (1 row per run)
results=pd.DataFrame()
results['patient count per week'] = (np.array(patient_count) /
                                     model_run.data.data_collection_weeks)
results['demand met'] = np.array(demand_met) / np.array(patient_count)
results['demand met first time'] = (np.array(demand_met_first_time) /
                                    np.array(patient_count))
results['demand failed'] = np.array(demand_failed) / np.array(patient_count)
results['available gp hours no overtime'] = available_gp_hours_no_overtime
results['used gp hours inc overtime'] = used_gp_hours
results['gp overtime'] = used_gp_overtime
results['prop failed triage'] = prop_failed_triage
results['prop failed same day'] = prop_failed_same_day
results['prop_failed short term'] = prop_failed_short_term
results['prop failed long term'] = prop_failed_long_term
results['prop failed visit'] = prop_failed_long_term
results['prop used gp time triage'] = prop_used_gp_time_triage
results['prop used gp time same day'] = prop_used_gp_time_same_day
results['prop used gp time short term'] = prop_used_gp_time_short_term
results['prop used gp time long term'] = prop_used_gp_time_long_term
results['prop used gp time visit'] = prop_used_gp_time_visit


# Summarise results (and transpose from standard format),
# and round to three decimal places
results_summary = results.describe().T
results_summary = np.round(results_summary, 3)

# Save results
results.to_csv('results.csv')
results_summary.to_csv('results_summary.csv')
