import queue
import numpy as np
from data import Data
from patient import Patient
from appointments import Appointment_diary

class Model():
    def __init__(self):
        """Constructor method for main model.
        A single instance is created."""
        
        # Load and parse data in Data class
        self.data = Data()
        self.appointments_diary = \
            Appointment_diary(self.data.diary_length,
                              self.data.gp_hours,
                              self.data.reserved_diary_by_type)

        # Initiate a prioirty queue for patients waiting for triage and
        # allocation. Priority queue is used to allow priority to be added
        # later if wanted (currently sorted by patient ID, so effectively a FIFO
        # queue

        self.patients = queue.PriorityQueue()

        # Patient counts
        self.patient_count = 0
        self.results_patient_count = 0
        self.demand_met = 0
        self.demand_met_first_time = 0
        self.demand_failed = 0

        # Weekday count (0=Monday, 6=Sunday)
        weekday = 0

        # Begin loop for individual days
        for day in range(self.data.model_duration):

            # Use FIFO queue for failed patients,to later 'recycle' the next day
            self.patients_demand_failed = queue.Queue()

            # Assign number of calls by Poisson distrubution around day mean
            calls = np.random.poisson(self.data.mean_calls_by_day[weekday])

            # Add new patients to queue
            for call in range(calls):

                self.patient_count += 1
                if day > self.data.model_warm_up_period:
                    self.results_patient_count += 1

                # Create patient object
                patient = Patient(self.patient_count,
                                  self.data.demand_type_cum_probability,
                                  self.data.triage_use_probability,
                                  self.data.triage_conversion_probabilities,
                                  self.data.appointment_durations,
                                  self.data.overtime_for_triage,
                                  self.data.overtime_for_same_day,
                                  self.data.failed_demand_return_proportion)

                # Create tuple to add to priority queue
                patient_tuple = (1, self.patient_count, patient)
                self.patients.put(patient_tuple)

            # Take patients from queue and assign appointments
            while not self.patients.empty():
                patient_tuple = self.patients.get()
                patient = patient_tuple[2]

                self.appointments_diary.assign_appointment(
                    day, patient, self.data.appointment_booking_windows)

                if patient.failed_to_get_appointment == 1:
                    self.patients_demand_failed.put(patient)
                else:
                    if day > self.data.model_warm_up_period:
                        self.demand_met += 1
                        if patient.attempts == 1:
                            self.demand_met_first_time +=1


            # Process failed paitients
            # (place back in main queue if calling next day)

            while not self.patients_demand_failed.empty():
                patient = self.patients_demand_failed.get()
                if patient.return_next_day == 1:
                    patient.attempts += 1
                    patient_tuple = (1, patient.id, patient)
                    self.patients.put(patient_tuple)
                else:
                    if day > self.data.model_warm_up_period:
                            self.demand_failed += 1

            # Increment weekday
            weekday += 1
            if weekday == 7:
                weekday = 0

        # Get appointment type results
        # (get diary events during data collection period only)
        first_row = self.data.model_warm_up_period
        last_row = (self.data.model_warm_up_period +
                    self.data.data_collection_weeks * 7 + 1)

        results_used_gp_by_type = \
            self.appointments_diary.gp_used_hours[first_row:last_row]

        results_succesful_appointemnts_by_type = \
            self.appointments_diary.gp_booked_appointments[first_row:last_row]

        results_failed_appointments_by_type = \
            self.appointments_diary.gp_rejected_appointments

        self.results_proportion_of_appointments_failed_by_type = (
            results_failed_appointments_by_type.sum(axis=0) /
            (results_failed_appointments_by_type.sum(axis=0) +
             results_succesful_appointemnts_by_type.sum(axis=0)))

        self.results_proportion_used_GP_time_used_by_type = (
            results_used_gp_by_type.sum(axis=0) / results_used_gp_by_type.sum())
        
        self.results_total_gp_hours_used = np.sum(
            self.appointments_diary.gp_used_hours[first_row:last_row])

        self.results_total_gp_overtime_hours_used = np.sum(
            self.appointments_diary.gp_overtime[first_row:last_row])

        return
