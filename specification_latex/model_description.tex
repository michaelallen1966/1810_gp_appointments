\documentclass{article}
\usepackage{url}
\usepackage{microtype}
\usepackage{parskip}
\usepackage[super]{natbib}
\usepackage[a4paper, left=2.5cm, right=2.5cm, top=2.5cm, bottom=2.5cm]{geometry}
\usepackage{longtable,booktabs}
\usepackage{caption}
\usepackage{blindtext}
\usepackage{graphicx}
\usepackage{authblk}

\begin{document}

\title{Modelling general practice access/appointment systems: model description} 

\author[1]{Michael Allen, PhD}

\affil[1]{University of Exeter Medical School \& NIHR Collaboration for Leadership in Applied Health Research and Care  South West Peninsula.}

\renewcommand\Authands{ and }

\date{October 2018}
\maketitle

\section*{Abstract} % Use * to avoid numbering of section

This model, which requires a python installation to run (e.g. see /url{www.anaconda.org} for a free distribution of scientific python for Mac, Windows or Linux), models the performance of a GP appointments system. It allows for four different appointment types (same-day, short-term advance booking, long-term-advance booking, and visits), each of which may be preceded by triage (e.g. phone triage). The number of average contacts per week, and the available GP time is entered, along with the average GP time taken for different appointment types. The diary may be blocked out with reserved time for particular appointment types. For triage and same-day appointments GP overtime may be used to meet demand if necessary. The model reports the GP utilization, GP overtime used, and the proportion of patients whose demand is met (along with the proportion of patients whose demand is met on first contact, as patients who fail to receive triage/appointments may return the next day).

\tableofcontents


\section{Background}
Managing demand and appointments in a GP practice is a perennial challenge. 
GP practices have a choice of various strategies including advance booking, on-the-day booking, and triage (which may take varying forms).

The aim of this model is to allow a comparison of the likely performance of different systems, predicting the percentage of time met, the amount of GP times used, and the amount of GP overtime used (if using overtime to cover excess demand is an option).

\section{Appointment types}
The model contains the option for the following appointment types. 
The booking system may reserve a proportion of each day for different appointment types (any non-reserved time is available to all appointment types).
Each appointment type may use a different amount of time with specified variation). 

\begin{enumerate}
\item \emph{Triage}. Triage may be performed by any means (e.g. phone/e-mail). The aim is to have a rapid assessment before deciding whether a patient needs to be seen (the patient may then be assigned to another appointment type).
\item \emph{Same-day}. Same-day appointments may only be assigned on the day of patient contact (with or without prior triage).
\item \emph{Short term advanced-booked}. These appointments may be made within a specified advance window (by default up to one week in advance).
\item \emph{Long-term scheduled}. These appointments may be made within a specified advance window (by default 1-4 weeks in advance).
\item \emph{Visit}. These appointments may be made within a specified advance window (by default up to one week in advance).
\end{enumerate}

\section{Model overview}

Figure \ref{fig:process} shows a high level overview of the model. 

The model assumes all calls come in early morning (or that all calls are made so that if there is available GP time that day it may be assigned on that day). The model then allows a triage step to be applied. Triage may be applied with varying proportion to different appointment types. For example it may be assumed that triage is never applied to people booking in advance, but is applied to 90\% of patients given on-the-day appointments. Triage will have a given conversion rate to face-to-face appointments (the conversion rate may vary by appointment type). If triage suggests a face-to-face appointment then there is an attempt to book the patient into the diary. If there is no time available in the GP diary then the patient is informed to call back the next day if they still want to see a GP. There is also an option in the model to allocate a given percentage of on-the-day patients to GP overtime (e.g. it may be considered that 50\% of patients where triage suggests face-to-face, but no time is available in the regular diary, should be seen at the end of the day by allocating GP overtime). If overtime is used then this is tracked in the model.   

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{process.png}
\caption{Model overview}
\label{fig:process}
\end{figure}

\section{Model inputs}

The model takes the following inputs:

\begin{enumerate}
\item Average number of calls each week.
\item Spread of calls (as percentage of total weekly calls) across week.
\item GP hours available by day of week.
\item Proportion of face-to-face appointments, on average, assigned to same-day, short-term appointments, long-term appointments, or visits.
\item Allow GP overtime for same-day appointments (yes/no), and percentage of patients allocated to face-to-face by triage where overtime would be used rather than asking patient to call back next day.
\item Triage used (yes/no), and proportion of calls suitable for triage by appointment type.
\item Conversion rate for triage by appointment type.
\item The proportion of patients who are asked to call back tomorrow who actually call back tomorrow.
\item Average time required for triage and face-to-face appointments (may be varied by appointment type).
\item Optional: proportion of GP time reserved for different face-to-face appointment types (any non-reserved time is available for any appointment type). 
\end{enumerate}


\section{Model inputs}

Model inputs are set in the \emph{input.csv} file. The input sections are:

\begin{itemize}

\item \emph{WEEKLY DEMAND}: This is the total number of initial patient contacts per week. Each contact will, resource allowing, result in a patient triage contact (when triage is used) and then, as necessary, a face-to-face appointment. People having to call back next day due to failure to make appointment are not included in this number.

\item \emph{DEMAND SPLIT ACROSS WEEK}: This describes how the weekly contacts are distributed across the week. It should always add up to 1.0. 

\item \emph{DEMAND TYPE}: This describes the split of patients according to which type of appointment they wish to make.

\item \emph{USE OF TRIAGE}: This describes the proportion of patients, by desired appointment type, that are allocated to first receiving triage. For example, for some long-term appointments the receptionist may know that is a required appointment as part of their care plan, for others the receptionist may first assign the patient to triage to establish whether or not a face-to-face appointment is needed.

\item \emph{TRIAGE CONVERSION RATE}: This is the proportion of triaged patients, by appointment type, who are assigned a subsequent face-to-face appointment.

\item \emph{DEMAND FAILURE}: This describes what happens when there is insufficient GP diary time to triage or see a patient. \emph{Prop triage using overtime} describes the proportion of patients requiring triage where the GP will use overtime to perform the triage. \emph{Prop same day using overtime} describes the proportion of patients requiring a same-day appointment where the GP will use overtime to perform the triage. \emph{Prop failed demand who call next day} describes the proportion of patients who will call back the next day if no appointment was available in their current contact (note: it is assumed that if a patient has received triage then they will not need a subsequent triage; they may call back and say they have been told they should be seen face-to-face).

\item \emph{SCHEDULED APPOINTMENT DURATION}: This is the GP time, on average and in minutes, used for each patient. This time should also include any necessary paperwork, etc. For example, if a GP can see 5 patients per hour then this should be set at 12 minutes (even if the the expected patient contact time is 10 minutes).

\item \emph{DIARY WINDOW FOR APPOINTMENTS}: This section describes the allowed booking period for different types of appointment, with 0 being the same day. For example, same-day appointments will have both a minimum and maximum booking window of 0, that is the only diary time available to use is that of today. A long term booking window may have minimum and maximum booking window values of 7 and 27. This would mean that the first appointment slot that may be offered is one week from today, and the latest appointment booking slot that may be offered is 4 weeks from today (the patient will be booked into the earliest available slot).

\item \emph{PROPORTION OF DIARY RESERVED}: This section describes the proportion of each day's diary reserved for different appointment types. Unreserved time is available to any appointment type. For example, if Triage and Same-day reserved proportion are set at 0.2 and 0.3 respectively (and others set at zero), this will mean that 20\% of the day's available GP hours may only be used for triage, and another 30\% may only be used for same-day appointments. The remaining 50\% may be used by any appointment type (including triage and same-day appointments) on a simple 'first-come-first-served' basis. When booking appointments reserved time, if available, is used first. If relevant reserved time is fully booked (or no time has been reserved for a particular type of appointment), unreserved time may be used. If no unreserved time is available then overtime may be used for triage or same-day appointments (see above).

 \item \emph{GP CONTACT HOURS AVAILABLE}: This describes the total GP time available for appointments each day (it does not include other GP work or breaks). For example, if there are three GPs working on a Monday, and they have 7, 7 and 5 hours dedicated to patient appointments that day, then the GP contact hours would be 19. 

\end{itemize}


\section{Model outputs}

The duration of results collection is 12 weeks. Results collection starts after the warm-up period which allows pre-booked appointments to already be scheduled in the diary (as the model starts with an empty diary). The model is run as a trial series of independents run (default is 5 runs). Using a trial helps to show expected variation over the model run period, and avoids inaccuracies in modelling due to a single run.

Model outputs are in two files: /emph{results.csv}, which shows results from each trial, and \emph{results\_summary.csv}, which summarises results across the multiple trial runs. Results shown are:

\begin{itemize}
\item \emph{patient count per week}: number of original contacts per week (does not include repeat attempts across two or more days.

\item \emph{demand met}: the proportion of patients who receive required triage/appointment.

\item \emph{demand met first time}: the proportion of patients who receive required triage/appointment on their first contact with the GP surgery.

\item \emph{demand failed}: the proportion of patients who did not receive required triage/appointment and abandoned attempts.

\item \emph{available gp hours no overtime}: the number of GP hours available per week, excluding any overtime used.

\item \emph{used gp hours inc overtime}: the number of GP hours used per week, including any overtime used. Unused reserved diary time is not included. 

\item \emph{gp overtime}: the average number of overtime hours used per week.

\item \emph{prop failed triage} .. \emph{prop failed visit}: the proportion of patients requesting different appointment types who did not receive an appointment, and who gave up.

\item \emph{prop used gp time triage} .. \emph{prop gp time visit}: the proportion of actual used GP time (including overtime) spent on each type of appointment. Unused reserved diary time is not included.

\end{itemize}


\section{Model assumptions and simplifications}
This model makes various assumptions for the sake of simplicity:
\begin{itemize}
\item The model only takes into account GP time; it does not model other healthcare practitioners.
\item The model assumes all appointment calls are made in time to allocate to 'one-the-day' appointments if time is available.
\item Patients may be seen by any GP.
\item Seasonal variation is not included in the model (but the model could be re-run with data representative of different seasons).
\item Weekends may be included in the model, but bank holidays are not included.
\item Variation in demand between the same weekday in different weeks is assumed to be described by a Poison distribution.
\item The model does not include 'special cause' variation in demand outside of normal variation (such as an acute epidemic).
\end{itemize}



\end{document}
